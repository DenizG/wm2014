#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pa2proj.settings")

from django.conf import settings
from django.db import models, DatabaseError
from django.utils.timezone import make_aware, get_current_timezone
from wm2014.models import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class GroupModel(QAbstractTableModel):

	HEADER = [
		u'Nationalität',
		u'Kontinent',
		u'ISO-3166-1',
		u'GS',
		u'S',
		u'U',
		u'N',
		u'T',
		u'GT',
		u'Punkte',
	]

	def __init__(self, letter):
		super(GroupModel, self).__init__(None) 

		try:
			self.group = Group.objects.get(letter = letter)

			self.team1 = self.group.team1
			self.team2 = self.group.team2
			self.team3 = self.group.team3
			self.team4 = self.group.team4

		except Group.DoesNotExist:
			self.group = None
			pass

	def rowCount(self, parent):
		return 4 # teams

	def columnCount(self, parent):
		return len(self.HEADER)

	def setData(self, index, value, role):
		try:
			if role == Qt.EditRole:
				team = self.__teamFromRow__(index.row())
				if self.__isNationality__(index):
					team.nationality.name = unicode(value.toString())
					team.nationality.save()
					return True
				elif self.__isContinent__(index):
					team.nationality.continent = unicode(value.toString())
					team.nationality.save()
					return True
				elif self.__isShortcut__(index):
					team.nationality.shortcut = unicode(value.toString())
					team.nationality.save()
					return True
			return False
		except DatabaseError as e:
			print "Datenbankfehler: ", e
			return False

	def headerData(self, col, orientation, role):
		if orientation == Qt.Horizontal and role == Qt.DisplayRole:
			return QVariant(self.HEADER[col])
		return QVariant()

	def data(self, index, role):
		if role == Qt.DisplayRole and self.group != None:
			team = self.__teamFromRow__(index.row())
			return self.__dataFromColumn__(index.column(), team)

	def flags(self, index):
		f = Qt.ItemIsEnabled | Qt.ItemIsUserCheckable
		if self.__isEditable__(index):
			return f | Qt.ItemIsEditable
		else:
			return f

	def __dataFromColumn__(self, col, team):
		result = {
			0: lambda team: unicode(team.nationality),
			1: lambda team: unicode(team.nationality.continent),
			2: lambda team: unicode(team.nationality.shortcut),
			3: lambda team: team.played(),
			4: lambda team: team.win(),
			5: lambda team: team.draw(),
			6: lambda team: team.loss(),
			7: lambda team: team.goals(),
			8: lambda team: team.agoals(),
			9: lambda team: team.points()
		}[col](team)
		return QVariant(result)

	def __teamFromRow__(self, row):
		return {
			0: self.group.team1,
			1: self.group.team2,
			2: self.group.team3,
			3: self.group.team4,
		}[row]

	def __isEditable__(self, index):
		return self.__isNationality__(index) or self.__isContinent__(index) or self.__isShortcut__(index)

	def __isNationality__(self, index):
		return index.column() == 0

	def __isContinent__(self, index):
		return index.column() == 1

	def __isShortcut__(self, index):
		return index.column() == 2

def createGroupTab(letter):
	w = QWidget()
	l = QGridLayout(w)
	tGroup.addTab(w, 'Gruppe ' + letter)
	
	tm = GroupModel(letter)
	tv = QTableView()
	tv.setModel(tm)
	tv.resizeColumnsToContents()

	l.addWidget(tv)

class GameItem(QListWidgetItem):
	def __init__(self, game):
		QListWidgetItem.__init__(self, unicode(game), None)
		self.game = game

class GameListWidget(QListWidget):
	def __init__(self, gameDetailWidget, goalModel, goalView):
		QListWidget.__init__(self, None)

		allGames = Game.objects.all()

		for g in allGames:
			gi = GameItem(g)
			self.addItem(gi)

		if len(allGames) > 0:
			gameDetailWidget.show(allGames[0])

		self.gameDetailWidget = gameDetailWidget
		self.goalModel = goalModel
		self.goalView = goalView
		self.currentItemChanged.connect(self.onGameClicked)

	def onGameClicked(self, current, previous):
		self.gameDetailWidget.show(current.game)
		self.goalModel.choose(current.game)
		self.goalView.setModel(None)
		self.goalView.setModel(self.goalModel)

class GameDetailWidget(QGroupBox):

	def __init__(self):
		QGroupBox.__init__(self, None)

		self.lDate    = QDateEdit(self)
		self.lStadium = QLineEdit("", self)
		self.lNature  = QLabel("", self)
		self.lTeam1   = QComboBox(self)
		self.lTeam2   = QComboBox(self)

		self.listOfQualifiedTeams = self.__createListOfQualifiedTeams__()
		[self.lTeam1.addItem(unicode(t)) for t in self.listOfQualifiedTeams.keys()]
		[self.lTeam2.addItem(unicode(t)) for t in self.listOfQualifiedTeams.keys()]

		self.lDate.dateTimeChanged.connect(self.onDateEdit)
		self.lStadium.textEdited.connect(self.onStadiumEdit)
		self.lTeam1.currentIndexChanged['QString'].connect(self.onTeam1Edit)
		self.lTeam2.currentIndexChanged['QString'].connect(self.onTeam2Edit)

		l = QGridLayout(self)
		l.addWidget(QLabel("Datum")  , 0, 0)
		l.addWidget(QLabel("Stadium"), 1, 0)
		l.addWidget(QLabel("Art")    , 2, 0)
		l.addWidget(QLabel("Team 1") , 3, 0)
		l.addWidget(QLabel("Team 2") , 4, 0)

		l.addWidget(self.lDate   , 0, 1)
		l.addWidget(self.lStadium, 1, 1)
		l.addWidget(self.lNature , 2, 1)
		l.addWidget(self.lTeam1  , 3, 1)
		l.addWidget(self.lTeam2  , 4, 1)

	def show(self, game):
		self.lDate.setDate(game.date.date())
		self.lStadium.setText(unicode(game.stadium))
		self.lNature.setText(game.nature)

		t1Index = self.lTeam1.findText(unicode(game.team1.nationality))
		t2Index = self.lTeam2.findText(unicode(game.team2.nationality))

		self.lTeam1.setCurrentIndex(t1Index)
		self.lTeam2.setCurrentIndex(t2Index)
		
		self.currentGame = game

	def __createListOfQualifiedTeams__(self):
		listOfListsOfTeams = map(lambda g: g.teams(), Group.objects.all())
		listOfTeams = [team for teamList in listOfListsOfTeams for team in teamList]
		teams = {}
		for team in listOfTeams:
			teams[team.nationality.name] = team
		return teams

	def onDateEdit(self, date):
		try:
			print "TODO: GameDetailWidget::onDateEdit({})".format(date)
			#newDateTime = make_aware(date.toPyDateTime(), get_current_timezone())
			#self.currentGame.date = newDateTime
			#self.currentGame.save()
		except AttributeError:
			pass

	def onStadiumEdit(self, stadium):
		try:
			self.currentGame.stadium = stadium
			self.currentGame.save()
		except AttributeError:
			pass

	def onTeam1Edit(self, team):
		try:
			dbTeam = self.listOfQualifiedTeams[unicode(team)]
			self.currentGame.team1 = dbTeam
			self.currentGame.save()
		except AttributeError:
			pass

	def onTeam2Edit(self, team):
		try:
			dbTeam = self.listOfQualifiedTeams[unicode(team)]
			self.currentGame.team2 = dbTeam
			self.currentGame.save()
		except AttributeError:
			pass

class GoalModel(QAbstractTableModel):

	HEADER = [
		u'Minute',
		u'Spieler',
		u'Team',
	]

	goalData = []

	def __init__(self):
		super(GoalModel, self).__init__(None)

	def choose(self, game):
		goals = game.goal_set.all()
		self.goalData = goals.values_list('minute', 'player', 'team_id')

	def rowCount(self, parent):
		return len(self.goalData)

	def columnCount(self, parent):
		return len(self.HEADER)

	def setData(self, index, value, role):
		try:
			if role == Qt.EditRole:
				print "TODO: GoalModel::setData()"
			return False
		except DatabaseError as e:
			print "Datenbankfehler: ", e
			return False

	def headerData(self, col, orientation, role):
		if orientation == Qt.Horizontal and role == Qt.DisplayRole:
			return QVariant(self.HEADER[col])
		return QVariant()

	def data(self, index, role):
		if role == Qt.DisplayRole:
			return self.goalData[index.row()][index.column()]

	def flags(self, index):
		return Qt.ItemIsEnabled | Qt.ItemIsUserCheckable | Qt.ItemIsEditable

class GoalWidget(QTableView):

	def __init__(self):
		QListWidget.__init__(self, None)

# Main Window
a = QApplication(sys.argv)
wApp = QWidget()
lApp = QGridLayout(wApp)

# Groups
wGroup = QWidget()
lGroup = QGridLayout(wGroup)
tGroup = QTabWidget()
lGroup.addWidget(tGroup)

# Games
wGame = QWidget()
lGame = QHBoxLayout(wGame)

tDetail = GameDetailWidget()
mGoal = GoalModel()
tGoal = GoalWidget()
tGoal.setModel(mGoal)
wRightSide = QWidget()
lRightSide = QVBoxLayout(wRightSide)
lRightSide.addWidget(tDetail)
lRightSide.addWidget(tGoal)

tList = GameListWidget(tDetail, mGoal, tGoal)

lGame.addWidget(tList)
lGame.addWidget(wRightSide)

# Goals
#wGoal = QWidget()
#lGoal = QGridLayout(wGoal)
#tGoal = QTabWidget()
#lGoal.addWidget(tGoal)

tMain = QTabWidget()
lApp.addWidget(tMain)
tMain.addTab(wGroup, 'Gruppen')
tMain.addTab(wGame, 'Spiele')
#tMain.addTab(wGoal, 'Tore')

# ------------------------------------------------

map(createGroupTab, 'ABCDEFGH')

dbWm = WM.objects.get()

wApp.resize(700, 500)
wApp.setWindowTitle("Weltmeisterschaft 2014 in " + dbWm.host.name)
wApp.show()

sys.exit(a.exec_())

