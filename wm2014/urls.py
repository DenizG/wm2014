from django.conf.urls import patterns, url
from wm2014 import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^group/?$', views.group, name='group'),
	url(r'^group/detail/(?P<letter>[A-H])/?$', views.groupDetail, name='groupDetail'),
	url(r'^game/(?P<number>\d{1,2})/?$', views.game, name='game'),
	url(r'^finals$/?$', views.finals, name='finals'),
)
