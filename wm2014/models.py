from django.db import models
from django.db.models import Q

MAX_CHARS_CONTINENT_NAME = 20
MAX_CHARS_GAME_NATURE = 2
MAX_CHARS_GROUP_NAME = 1
MAX_CHARS_NATION_NAME = 100
MAX_CHARS_NATION_SHORTCUT = 20
MAX_CHARS_STADIUM_NAME = 100
MAX_CHARS_PLAYER_NAME = 100

GAME_NATURE = (
	('G', 'Group'),
	('E1', 'Eight-Finals 1'),
	('E2', 'Eight-Finals 2'),
	('E3', 'Eight-Finals 3'),
	('E4', 'Eight-Finals 4'),
	('E5', 'Eight-Finals 5'),
	('E6', 'Eight-Finals 6'),
	('E7', 'Eight-Finals 7'),
	('E8', 'Eight-Finals 8'),
	('Q1', 'Quarter-Finals 1'),
	('Q2', 'Quarter-Finals 2'),
	('Q3', 'Quarter-Finals 3'),
	('Q4', 'Quarter-Finals 4'),
	('H1', 'Half-Finals 1'),
	('H2', 'Half-Finals 2'),
	('F1', 'Finals (First Place)'),
	('F3', 'Finals (Third Place'),
)

class Nation(models.Model):
	name = models.CharField(max_length=MAX_CHARS_NATION_NAME, unique=True)
	continent = models.CharField(max_length=MAX_CHARS_CONTINENT_NAME)
	shortcut = models.CharField(max_length=MAX_CHARS_NATION_SHORTCUT)

	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.name

class WM(models.Model):
	host = models.ForeignKey(Nation)
	begin = models.DateTimeField()
	end = models.DateTimeField()

	def __unicode__(self):
		return u'WM {}'.format(self.begin.year)

	def __str__(self):
		return 'WM {}'.format(self.begin.year)

class Team(models.Model):
	nationality = models.ForeignKey(Nation)

	def draw(self):
		gamesAsTeam1 = Game.objects.filter(team1 = self)
		gamesAsTeam2 = Game.objects.filter(team2 = self)
		drawsAsTeam1 = [g for g in gamesAsTeam1 if g.goals1() == g.goals2()]
		drawsAsTeam2 = [g for g in gamesAsTeam2 if g.goals2() == g.goals1()]
		return len(drawsAsTeam1) + len(drawsAsTeam2)

	def win(self):
		gamesAsTeam1 = Game.objects.filter(team1 = self)
		gamesAsTeam2 = Game.objects.filter(team2 = self)
		winsAsTeam1 = [g for g in gamesAsTeam1 if g.goals1() > g.goals2()]
		winsAsTeam2 = [g for g in gamesAsTeam2 if g.goals2() > g.goals1()]
		return len(winsAsTeam1) + len(winsAsTeam2)

	def loss(self):
		gamesAsTeam1 = Game.objects.filter(team1 = self)
		gamesAsTeam2 = Game.objects.filter(team2 = self)
		lossesAsTeam1 = [g for g in gamesAsTeam1 if g.goals1() < g.goals2()]
		lossesAsTeam2 = [g for g in gamesAsTeam2 if g.goals2() < g.goals1()]
		return len(lossesAsTeam1) + len(lossesAsTeam2)

	def played(self):
		return Game.objects.filter(Q(team1 = self) | Q(team2 = self)).count()

	def goals(self):
		return Goal.objects.filter(team = self).count()

	def agoals(self):
		participatedGames = Game.objects.filter(Q(team1 = self) | Q(team2 = self))
		listOfListsOfGoals = [g.goal_set.values_list('team', flat=True) for g in participatedGames]
		listOfGoals = [goal for goalList in listOfListsOfGoals for goal in goalList]
		return len([t for t in listOfGoals if t != self.pk])

	def points(self):
		return self.win() * 3 + self.draw()

	def __unicode__(self):
		return self.nationality.name

	def __str__(self):
		return self.nationality.name

class Group(models.Model):
	wm = models.ForeignKey(WM)
	letter = models.CharField(max_length=MAX_CHARS_GROUP_NAME, unique=True)
	team1 = models.ForeignKey(Team, related_name='group_team1')
	team2 = models.ForeignKey(Team, related_name='group_team2')
	team3 = models.ForeignKey(Team, related_name='group_team3')
	team4 = models.ForeignKey(Team, related_name='group_team4')

	def teams(self):
		teams = [self.team1, self.team2, self.team3, self.team4]
		teams.sort(key = lambda x : x.points())
		teams.reverse()
		return teams

	def __unicode__(self):
		return u'Gruppe {}'.format(self.letter)

	def __str__(self):
		return 'Gruppe {}'.format(self.letter)

class Game(models.Model):
	date = models.DateTimeField()
	stadium = models.CharField(max_length=MAX_CHARS_STADIUM_NAME)
	nature = models.CharField(max_length=MAX_CHARS_GAME_NATURE, choices=GAME_NATURE)
	group = models.ForeignKey(Group, null=True, blank=True)

	team1 = models.ForeignKey(Team, related_name='game_team1')
	team2 = models.ForeignKey(Team, related_name='game_team2')

	def number(self):
		games = list(Game.objects.all().order_by('date'))
		return 1 + [i for i,g in enumerate(games) if g == self][0]

	def goals1(self):
		return Goal.objects.filter(game = self, team = self.team1).count()

	def goals2(self):
		return Goal.objects.filter(game = self, team = self.team2).count()

	def __unicode__(self):
		return u'{} vs {} ({})'.format(self.team1, self.team2, self.date.date().strftime("%d.%m.%y"))

	def __str__(self):
		return '{} vs {} ({})'.format(self.team1, self.team2, self.date.date().strftime("%d.%m.%y"))

class Goal(models.Model):
	game = models.ForeignKey(Game)
	team = models.ForeignKey(Team)
	minute = models.IntegerField()
	player = models.CharField(max_length=MAX_CHARS_PLAYER_NAME)

