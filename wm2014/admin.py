from django.contrib import admin
from wm2014.models import *

class NationAdmin(admin.ModelAdmin):
    list_display = ('name', 'continent')

class WmAdmin(admin.ModelAdmin):
    pass

class TeamAdmin(admin.ModelAdmin):
    list_display = ('nationality',)

class GoalAdmin(admin.ModelAdmin):
    list_display = ('game', 'team', 'minute', 'player')

class GameAdmin(admin.ModelAdmin):
    list_display = ('date', 'stadium', 'group', 'team1', 'team2')

class GroupAdmin(admin.ModelAdmin):
    list_display = ('letter',)

admin.site.register(Nation, NationAdmin)
admin.site.register(WM, WmAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Goal, GoalAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(Group, GroupAdmin)
