from django.test import TestCase
from django.test import Client
from django.utils import timezone

from wm2014.models import *

def createWm():
	n1 = Nation(name = 'n1', continent = 'c1', shortcut = 's1')
	n2 = Nation(name = 'n2', continent = 'c2', shortcut = 's2')
	n3 = Nation(name = 'n3', continent = 'c3', shortcut = 's3')
	n4 = Nation(name = 'n4', continent = 'c4', shortcut = 's4')
	n1.save()
	n2.save()
	n3.save()
	n4.save()

	t1 = Team(nationality = n1)
	t2 = Team(nationality = n2)
	t3 = Team(nationality = n3)
	t4 = Team(nationality = n4)
	t1.save()
	t2.save()
	t3.save()
	t4.save()

	wm = WM(host = n1, begin = timezone.now(), end = timezone.now())
	wm.save()

	g = Group(wm = wm, letter = 'A', team1 = t1, team2 = t2, team3 = t3, team4 = t4)
	g.save()

def createGames():
	group1 = Group.objects.get(letter = 'A')
	team1 = Team.objects.get(nationality = Nation.objects.get(name = 'n1'))
	team2 = Team.objects.get(nationality = Nation.objects.get(name = 'n2'))
	game1 = Game(date = timezone.now(), stadium = 's1', nature = 'G', group = group1, team1 = team1, team2 = team2)
	game2 = Game(date = timezone.now(), stadium = 's2', nature = 'G', group = group1, team1 = team2, team2 = team1)
	game3 = Game(date = timezone.now(), stadium = 's3', nature = 'G', group = group1, team1 = team2, team2 = team1)
	game4 = Game(date = timezone.now(), stadium = 's4', nature = 'G', group = group1, team1 = team2, team2 = team1)
	game1.save()
	game2.save()
	game3.save()
	game4.save()

	goal1 = Goal(game = game1, team = team1, minute = 51, player = 'Bongo Christ')
	goal2 = Goal(game = game1, team = team1, minute = 13, player = 'Creedence Clearwater Couto')
	goal3 = Goal(game = game1, team = team2, minute = 29, player = 'Norman Conquest')
	goal4 = Goal(game = game2, team = team2, minute = 89, player = 'Njube Sundowns')
	goal5 = Goal(game = game2, team = team2, minute = 85, player = 'Danger Fourpence')
	goal6 = Goal(game = game3, team = team1, minute = 1, player = 'Anthony Philip David Terry Frank Donald Stanley Gerry Gordon Stephen James Oatway')
	goal7 = Goal(game = game3, team = team1, minute = 2, player = 'Anthony Philip David Terry Frank Donald Stanley Gerry Gordon Stephen James Oatway')
	goal1.save()
	goal2.save()
	goal3.save()
	goal4.save()
	goal5.save()
	goal6.save()
	goal7.save()

class ModelLogicTest(TestCase):
	def setUp(self):
		createWm()
		createGames()

		self.team1 = Team.objects.get(nationality = Nation.objects.get(name = 'n1'))
		self.team2 = Team.objects.get(nationality = Nation.objects.get(name = 'n2'))

	def testWins(self):
		win1 = self.team1.win()
		win2 = self.team2.win()

		self.assertEqual(win1, 2)
		self.assertEqual(win2, 1)

	def testLoss(self):
		loss1 = self.team1.loss()
		loss2 = self.team2.loss()

		self.assertEqual(loss1, 1)
		self.assertEqual(loss2, 2)

	def testDraw(self):
		draw1 = self.team1.draw()
		draw2 = self.team2.draw()

		self.assertEqual(draw1, 1)
		self.assertEqual(draw2, 1)

	def testLoss(self):
		loss1 = self.team1.loss()
		loss2 = self.team2.loss()

		self.assertEqual(loss1, 1)
		self.assertEqual(loss2, 2)


	def testPlayed(self):
		played1 = self.team1.played()
		played2 = self.team2.played()

		self.assertEqual(played1, 4)
		self.assertEqual(played2, 4)

	def testGameGoals(self):
		game1 = Game.objects.get(team1 = self.team1, team2 = self.team2, stadium = 's1')
		game2 = Game.objects.get(team1 = self.team2, team2 = self.team1, stadium = 's2')
		game3 = Game.objects.get(team1 = self.team2, team2 = self.team1, stadium = 's3')
		game4 = Game.objects.get(team1 = self.team2, team2 = self.team1, stadium = 's4')

		self.assertEqual(game1.goals1(), 2) # goals for n1
		self.assertEqual(game1.goals2(), 1) # goals for n2
		self.assertEqual(game2.goals1(), 2) # goals for n2
		self.assertEqual(game2.goals2(), 0) # goals for n1
		self.assertEqual(game3.goals1(), 0) # goals for n2
		self.assertEqual(game3.goals2(), 2) # goals for n1
		self.assertEqual(game4.goals1(), 0) # goals for n1
		self.assertEqual(game4.goals2(), 0) # goals for n2

	def testTeamGoals(self):
		goals1 = self.team1.goals()
		goals2 = self.team2.goals()

		self.assertEqual(goals1, 4)
		self.assertEqual(goals2, 3)

	def testTeamAgoals(self):
		agoals1 = self.team1.agoals()
		agoals2 = self.team2.agoals()

		self.assertEqual(agoals1, 3)
		self.assertEqual(agoals2, 4)

class SiteTest(TestCase):
	def setUp(self):
		createWm()
		createGames()

	def testIndex(self):
		c = Client()
		response = c.get('/wm2014/')

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')

		response = c.get('/wm2014')
		self.assertRedirects(response, '/wm2014/', status_code=301, target_status_code=200)

	def testShortcuts(self):
		g = Group.objects.get()

		self.assertEqual(g.team1.nationality.name, unicode(g.team1.nationality))

	def testGroup(self):
		c = Client()
		response = c.get('/wm2014/group')

		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'group.html')
		self.assertContains(response, 'n4')
		self.assertContains(response, 'c4')
		self.assertContains(response, 's4')

	def testGroupDetail(self):
		c = Client()

		# Not existing Group
		notFound = c.get('/wm2014/group/detail/B')
		self.assertEqual(notFound.status_code, 404)

		# Impossible Group
		notPossible = c.get('/wm2014/group/detail/Q')
		self.assertEqual(notPossible.status_code, 404)

		# Valid Group
		response = c.get('/wm2014/group/detail/A')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'groupDetail.html')
		self.assertContains(response, 'Gruppe A')
		self.assertContains(response, 's1')
		self.assertContains(response, 's2')
		self.assertContains(response, 'n1')

	def testGame(self):
		c = Client()

		# No game number
		noNumber = c.get('/wm2014/game')
		self.assertEqual(noNumber.status_code, 404)

		# Not existing Game
		notFound = c.get('/wm2014/game/42')
		self.assertEqual(notFound.status_code, 404)

		# Impossible Game
		notPossible = c.get('/wm2014/game/blah')
		self.assertEqual(notPossible.status_code, 404)

		# Valid Group
		response = c.get('/wm2014/game/1')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'game.html')
		#self.assertContains(response, 'Gruppe A')      # TODO
		self.assertContains(response, 's1')                         # stadium
		self.assertContains(response, 'n1')                         # team1
		self.assertContains(response, 'n2')                         # team2
		self.assertContains(response, '51')                         # minute
		self.assertContains(response, '13')                         # minute
		self.assertContains(response, '29')                         # minute
		self.assertContains(response, 'Bongo Christ')               # player
		self.assertContains(response, 'Creedence Clearwater Couto') # player
		self.assertContains(response, 'Norman Conquest')            # player

