from django.http import Http404
from django.shortcuts import render, get_object_or_404
from wm2014.models import *
from wm2014.plan import createPlan

def index(request):
	context = {}
	return render(request, 'index.html', context)

def group(request):

	groups = Group.objects.all().order_by('letter')

	context = {
		'groups' : groups
	}

	return render(request, 'group.html', context)

def groupDetail(request, letter):

	group = get_object_or_404(Group, letter = letter)
	games = Game.objects.filter(group = group).order_by('date')

	context = {
		'letter' : letter,
		'games' : games,
	}

	return render(request, 'groupDetail.html', context)

def game(request, number):
	
	if int(number) > Game.objects.count():
		raise Http404

	games = Game.objects.all()
	game = (g for g in games if g.number() == int(number)).next()
	goals = Goal.objects.filter(game = game).order_by('minute')

	context = {
		'number' : number,
		'game' : game,
		'goals' : goals
	}

	return render(request, 'game.html', context)

def finals(request):

	plan = createPlan()
	groups = Group.objects.all().order_by('letter')

	context = {
		'eightFinals'   : plan['eightFinals'],
		'quarterFinals' : plan['quarterFinals'],
		'halfFinals'    : plan['halfFinals'],
		'third'         : plan['third'],
		'final'         : plan['final'],
		'groups'        : groups,
	}

	return render(request, 'finals.html', context)

