from wm2014.models import *

FINAL_DEFAULTS = {
	'E1' : { 'team1' : 'Erster Gruppe A', 'team2' : 'Zweiter Gruppe B', 'goals1' : None, 'goals2' : None },
	'E2' : { 'team1' : 'Erster Gruppe C', 'team2' : 'Zweiter Gruppe D', 'goals1' : None, 'goals2' : None },
	'E3' : { 'team1' : 'Erster Gruppe B', 'team2' : 'Zweiter Gruppe A', 'goals1' : None, 'goals2' : None },
	'E4' : { 'team1' : 'Erster Gruppe D', 'team2' : 'Zweiter Gruppe C', 'goals1' : None, 'goals2' : None },
	'E5' : { 'team1' : 'Erster Gruppe E', 'team2' : 'Zweiter Gruppe F', 'goals1' : None, 'goals2' : None },
	'E6' : { 'team1' : 'Erster Gruppe G', 'team2' : 'Zweiter Gruppe H', 'goals1' : None, 'goals2' : None },
	'E7' : { 'team1' : 'Erster Gruppe F', 'team2' : 'Zweiter Gruppe E', 'goals1' : None, 'goals2' : None },
	'E8' : { 'team1' : 'Erster Gruppe H', 'team2' : 'Zweiter Gruppe G', 'goals1' : None, 'goals2' : None },
	'Q1' : { 'team1' : 'Sieger AF 5'    , 'team2' : 'Sieger AF 6'     , 'goals1' : None, 'goals2' : None },
	'Q2' : { 'team1' : 'Sieger AF 1'    , 'team2' : 'Sieger AF 2'     , 'goals1' : None, 'goals2' : None },
	'Q3' : { 'team1' : 'Sieger AF 7'    , 'team2' : 'Sieger AF 8'     , 'goals1' : None, 'goals2' : None },
	'Q4' : { 'team1' : 'Sieger AF 3'    , 'team2' : 'Sieger AF 4'     , 'goals1' : None, 'goals2' : None },
	'H1' : { 'team1' : 'Sieger VF 1'    , 'team2' : 'Sieger VF 2'     , 'goals1' : None, 'goals2' : None },
	'H2' : { 'team1' : 'Sieger VF 3'    , 'team2' : 'Sieger VF 4'     , 'goals1' : None, 'goals2' : None },
	'F3' : { 'team1' : 'Verlierer HF 1' , 'team2' : 'Verlierer HF 2'  , 'goals1' : None, 'goals2' : None },
	'F1' : { 'team1' : 'Sieger HF 1'    , 'team2' : 'Sieger HF 2'     , 'goals1' : None, 'goals2' : None },
}

def gameOrDefault(nature):
	try:
		return Game.objects.get(nature = nature)
	except Game.DoesNotExist:
		return FINAL_DEFAULTS[nature]

def createPlan():
	e1 = gameOrDefault(nature = 'E1')
	e2 = gameOrDefault(nature = 'E2')
	e3 = gameOrDefault(nature = 'E3')
	e4 = gameOrDefault(nature = 'E4')
	e5 = gameOrDefault(nature = 'E5')
	e6 = gameOrDefault(nature = 'E6')
	e7 = gameOrDefault(nature = 'E7')
	e8 = gameOrDefault(nature = 'E8')

	q1 = gameOrDefault(nature = 'Q1')
	q2 = gameOrDefault(nature = 'Q2')
	q3 = gameOrDefault(nature = 'Q3')
	q4 = gameOrDefault(nature = 'Q4')

	h1 = gameOrDefault(nature = 'H1')
	h2 = gameOrDefault(nature = 'H2')

	f1 = gameOrDefault(nature = 'F1')
	f3 = gameOrDefault(nature = 'F3')

	return {
		'eightFinals'   : [e1, e2, e3, e4, e5, e6, e7, e8],
		'quarterFinals' : [q1, q2, q3, q4],
		'halfFinals'    : [h1, h2],
		'third'         : f3,
		'final'         : f1,
	}

