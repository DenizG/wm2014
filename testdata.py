# -*- coding: UTF-8 -*-
from wm2014.models import *

# TODO: TESTING
from django.utils import timezone

CONTINENTS = {
	'AF' : u'Afrika',
	'EU' : u'Europa',
	'NA' : u'Nordamerika',
	'SA' : u'Südamerika',
	'AU' : u'Australien',
	'AS' : u'Asien',
}

def createGroup(wm, letter, nations, continents, shortcuts):
	n1 = Nation(name = nations[0], continent = CONTINENTS[continents[0]], shortcut = shortcuts[0])
	n2 = Nation(name = nations[1], continent = CONTINENTS[continents[1]], shortcut = shortcuts[1])
	n3 = Nation(name = nations[2], continent = CONTINENTS[continents[2]], shortcut = shortcuts[2])
	n4 = Nation(name = nations[3], continent = CONTINENTS[continents[3]], shortcut = shortcuts[3])

	# evil hack
	if nations[0] == 'Brasilien':
		try:
			n = Nation.objects.get(name = 'Brasilien')
			n1 = n
		except DoesNotExist:
			n1.save()

	n1.save()
	n2.save()
	n3.save()
	n4.save()

	t1 = Team(nationality = n1)
	t2 = Team(nationality = n2)
	t3 = Team(nationality = n3)
	t4 = Team(nationality = n4)

	t1.save()
	t2.save()
	t3.save()
	t4.save()

	g1 = Group(wm = wm, letter = letter, team1 = t1, team2 = t2, team3 = t3, team4 = t4)
	g1.save()

host = Nation(name = u'Brasilien', continent = CONTINENTS['SA'], shortcut = 'br')
host.save()

# TODO
wm = WM(host = host, begin = timezone.now(), end = timezone.now())
wm.save()

createGroup(wm, 'A', [u'Brasilien', u'Kamerun', u'Kroatien', u'Mexiko'], ['SA', 'AF', 'EU', 'NA'], ['br', 'cm', 'hr', 'mx'] )
createGroup(wm, 'B', [u'Australien', u'Chile', u'Niederlande', u'Spanien'], ['AU', 'SA', 'EU', 'EU'], ['au', 'cl', 'nl', 'es'] )
createGroup(wm, 'C', [u'Elfenbeinküste', u'Griechenland', u'Japan', u'Kolumbien'], ['AF', 'EU', 'AS', 'SA'], ['ci', 'gr', 'jp', 'co'] )
createGroup(wm, 'D', [u'Costa Rica', u'England', u'Italien', u'Uruguay'], ['NA', 'EU', 'EU', 'SA'], ['cr', 'gb', 'it', 'uy'] )
createGroup(wm, 'E', [u'Ecuador', u'Frankreich', u'Honduras', u'Schweiz'], ['SA', 'EU', 'NA', 'EU'], ['ec', 'fr', 'hn', 'ch'] )
createGroup(wm, 'F', [u'Argentinien', u'Bosnien-Herzegowina', u'Iran', u'Nigeria'], ['SA', 'EU', 'AS', 'AF'], ['ar', 'ba', 'ir', 'ng'] )
createGroup(wm, 'G', [u'Deutschland', u'Ghana', u'Portugal', u'USA'], ['EU', 'AF', 'EU', 'NA'], ['de', 'gh', 'pt', 'us'] )
createGroup(wm, 'H', [u'Algerien', u'Belgien', u'Russland', u'Südkorea'], ['AF', 'EU', 'AS', 'AS'], ['dz', 'be', 'ru', 'kr'] )
