DROP DATABASE IF EXISTS wm2014;
CREATE DATABASE wm2014;
GRANT USAGE ON *.* TO 'wm2014'@'localhost';
DROP USER 'wm2014'@'localhost';
CREATE USER 'wm2014'@'localhost' IDENTIFIED BY 'wm2014';
GRANT ALL PRIVILEGES ON wm2014.* TO 'wm2014'@'localhost';
USE wm2014;
-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: wm2014
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add nation',7,'add_nation'),(20,'Can change nation',7,'change_nation'),(21,'Can delete nation',7,'delete_nation'),(22,'Can add wm',8,'add_wm'),(23,'Can change wm',8,'change_wm'),(24,'Can delete wm',8,'delete_wm'),(25,'Can add team',9,'add_team'),(26,'Can change team',9,'change_team'),(27,'Can delete team',9,'delete_team'),(28,'Can add group',10,'add_group'),(29,'Can change group',10,'change_group'),(30,'Can delete group',10,'delete_group'),(31,'Can add game',11,'add_game'),(32,'Can change game',11,'change_game'),(33,'Can delete game',11,'delete_game'),(34,'Can add goal',12,'add_goal'),(35,'Can change goal',12,'change_goal'),(36,'Can delete goal',12,'delete_goal');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$4RvL7tteI6yl$Lj9oABabjXXy9x067x8kSZsIgkWuPuSynzSimZ2oQzM=','2014-02-26 10:52:31',1,'wm2014','','','wm2014@dzgr.de',1,1,'2014-02-26 10:52:15');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-02-26 10:52:42',1,9,'25','Deutschland',3,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'nation','wm2014','nation'),(8,'wm','wm2014','wm'),(9,'team','wm2014','team'),(10,'group','wm2014','group'),(11,'game','wm2014','game'),(12,'goal','wm2014','goal');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('yzgjwekpsip0fm1niohocvkaijacnttu','YzRhZjZjYmYzZDUyN2FhOGUyMDQwNjlmODk4Y2Q4NGE0NDFlNGRiYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-12 10:52:31');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wm2014_game`
--

DROP TABLE IF EXISTS `wm2014_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wm2014_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `stadium` varchar(100) NOT NULL,
  `nature` varchar(2) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `team1_id` int(11) NOT NULL,
  `team2_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wm2014_game_5f412f9a` (`group_id`),
  KEY `wm2014_game_bfb2ba20` (`team1_id`),
  KEY `wm2014_game_bdec4cfd` (`team2_id`),
  CONSTRAINT `team2_id_refs_id_0ad4213a` FOREIGN KEY (`team2_id`) REFERENCES `wm2014_team` (`id`),
  CONSTRAINT `group_id_refs_id_424efeda` FOREIGN KEY (`group_id`) REFERENCES `wm2014_group` (`id`),
  CONSTRAINT `team1_id_refs_id_0ad4213a` FOREIGN KEY (`team1_id`) REFERENCES `wm2014_team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wm2014_game`
--

LOCK TABLES `wm2014_game` WRITE;
/*!40000 ALTER TABLE `wm2014_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `wm2014_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wm2014_goal`
--

DROP TABLE IF EXISTS `wm2014_goal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wm2014_goal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `minute` int(11) NOT NULL,
  `player` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wm2014_goal_65e12249` (`game_id`),
  KEY `wm2014_goal_95e8aaa1` (`team_id`),
  CONSTRAINT `game_id_refs_id_d643d974` FOREIGN KEY (`game_id`) REFERENCES `wm2014_game` (`id`),
  CONSTRAINT `team_id_refs_id_7e99cab5` FOREIGN KEY (`team_id`) REFERENCES `wm2014_team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wm2014_goal`
--

LOCK TABLES `wm2014_goal` WRITE;
/*!40000 ALTER TABLE `wm2014_goal` DISABLE KEYS */;
/*!40000 ALTER TABLE `wm2014_goal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wm2014_group`
--

DROP TABLE IF EXISTS `wm2014_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wm2014_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wm_id` int(11) NOT NULL,
  `letter` varchar(1) NOT NULL,
  `team1_id` int(11) NOT NULL,
  `team2_id` int(11) NOT NULL,
  `team3_id` int(11) NOT NULL,
  `team4_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `letter` (`letter`),
  KEY `wm2014_group_c792e51e` (`wm_id`),
  KEY `wm2014_group_bfb2ba20` (`team1_id`),
  KEY `wm2014_group_bdec4cfd` (`team2_id`),
  KEY `wm2014_group_c7e8b533` (`team3_id`),
  KEY `wm2014_group_7ec1771d` (`team4_id`),
  CONSTRAINT `team4_id_refs_id_270d99f4` FOREIGN KEY (`team4_id`) REFERENCES `wm2014_team` (`id`),
  CONSTRAINT `team1_id_refs_id_270d99f4` FOREIGN KEY (`team1_id`) REFERENCES `wm2014_team` (`id`),
  CONSTRAINT `team2_id_refs_id_270d99f4` FOREIGN KEY (`team2_id`) REFERENCES `wm2014_team` (`id`),
  CONSTRAINT `team3_id_refs_id_270d99f4` FOREIGN KEY (`team3_id`) REFERENCES `wm2014_team` (`id`),
  CONSTRAINT `wm_id_refs_id_a912b4d0` FOREIGN KEY (`wm_id`) REFERENCES `wm2014_wm` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wm2014_group`
--

LOCK TABLES `wm2014_group` WRITE;
/*!40000 ALTER TABLE `wm2014_group` DISABLE KEYS */;
INSERT INTO `wm2014_group` VALUES (1,1,'A',1,2,3,4),(2,1,'B',5,6,7,8),(3,1,'C',9,10,11,12),(4,1,'D',13,14,15,16),(5,1,'E',17,18,19,20),(6,1,'F',21,22,23,24),(8,1,'H',29,30,31,32);
/*!40000 ALTER TABLE `wm2014_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wm2014_nation`
--

DROP TABLE IF EXISTS `wm2014_nation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wm2014_nation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `continent` varchar(20) NOT NULL,
  `shortcut` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wm2014_nation`
--

LOCK TABLES `wm2014_nation` WRITE;
/*!40000 ALTER TABLE `wm2014_nation` DISABLE KEYS */;
INSERT INTO `wm2014_nation` VALUES (1,'Brasilien','Südamerika','br'),(2,'Kamerun','Afrika','cm'),(3,'Kroatien','Europa','hr'),(4,'Mexiko','Nordamerika','mx'),(5,'Australien','Australien','au'),(6,'Chile','Südamerika','cl'),(7,'Niederlande','Europa','nl'),(8,'Spanien','Europa','es'),(9,'Elfenbeinküste','Afrika','ci'),(10,'Griechenland','Europa','gr'),(11,'Japan','Asien','jp'),(12,'Kolumbien','Südamerika','co'),(13,'Costa Rica','Nordamerika','cr'),(14,'England','Europa','gb'),(15,'Italien','Europa','it'),(16,'Uruguay','Südamerika','uy'),(17,'Ecuador','Südamerika','ec'),(18,'Frankreich','Europa','fr'),(19,'Honduras','Nordamerika','hn'),(20,'Schweiz','Europa','ch'),(21,'Argentinien','Südamerika','ar'),(22,'Bosnien-Herzegowina','Europa','ba'),(23,'Iran','Asien','ir'),(24,'Nigeria','Afrika','ng'),(25,'Deutschland','Europa','de'),(26,'Ghana','Afrika','gh'),(27,'Portugal','Europa','pt'),(28,'USA','Nordamerika','us'),(29,'Algerien','Afrika','dz'),(30,'Belgien','Europa','be'),(31,'Russland','Asien','ru'),(32,'Südkorea','Asien','kr');
/*!40000 ALTER TABLE `wm2014_nation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wm2014_team`
--

DROP TABLE IF EXISTS `wm2014_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wm2014_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nationality_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wm2014_team_fd532a39` (`nationality_id`),
  CONSTRAINT `nationality_id_refs_id_70c5057f` FOREIGN KEY (`nationality_id`) REFERENCES `wm2014_nation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wm2014_team`
--

LOCK TABLES `wm2014_team` WRITE;
/*!40000 ALTER TABLE `wm2014_team` DISABLE KEYS */;
INSERT INTO `wm2014_team` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),(23,23),(24,24),(26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32);
/*!40000 ALTER TABLE `wm2014_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wm2014_wm`
--

DROP TABLE IF EXISTS `wm2014_wm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wm2014_wm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int(11) NOT NULL,
  `begin` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wm2014_wm_27f00f5d` (`host_id`),
  CONSTRAINT `host_id_refs_id_b20253d2` FOREIGN KEY (`host_id`) REFERENCES `wm2014_nation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wm2014_wm`
--

LOCK TABLES `wm2014_wm` WRITE;
/*!40000 ALTER TABLE `wm2014_wm` DISABLE KEYS */;
INSERT INTO `wm2014_wm` VALUES (1,1,'2014-02-26 10:52:20','2014-02-26 10:52:20');
/*!40000 ALTER TABLE `wm2014_wm` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-26 11:52:47
